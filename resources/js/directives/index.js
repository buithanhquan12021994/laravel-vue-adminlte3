import Vue from 'vue';
import ClickOutSide from './click-ouside.js';

Vue.directive('click-outside', ClickOutSide);
