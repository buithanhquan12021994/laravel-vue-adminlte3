// Modify Cookie Name and Local Storage Name
export const ApplicationStorageToken = "app.new_gate.token";
export const ApplicationStorageUserDataName = "app.new_gate.userData";
export const ApplicationIsAuth = 'app.new_gate.isAuth';
export const ApplicationExpiredAt = 'app.new_gate.expiredAt';
export const ApplicationLang = 'app.new_gate.language';
