import axios from 'axios'
import Constants from './constants.js'
import StorageManage from '../helpers/storageManage.js';
import { ApplicationLang } from '../plugins/settings'
import { buildHeaders, buildUrl, parseValidationMessage } from '../helpers/common.js';
import StatusCode from './statusCode.js';

export default class HttpClient {
    constructor(baseUrl) {
        this.axiosInstance = axios.create({
            baseURL: baseUrl,
            timeout: Constants.defaultRequestTimeOut,
            headers: {
                'accept': 'application/json',
                'content-type': 'application/json',
                'lang':  StorageManage.getStorage(ApplicationLang) || 'ja',
            }
        });
    }

    getOptions(header = {}) {
        var options = {
            headers: buildHeaders(header)
        };
        return options;
    }

    // Handle error
    handleError(err) {
        if (err.response) {
            let statusCodeResponse = err.response.status; // get status from err
            let structureError = {};
            // validation fail
            if (statusCodeResponse == StatusCode.HTTP_UNPROCESSABLE_ENTITY) {
                structureError.validateFail = true;
                structureError.data = parseValidationMessage(err.response.data.data);
                return structureError;
            }
            structureError.data = err.response.data.message || err.response.data.data || 'Unknow error!';

            return structureError;
        }

        return err;
    }

    // Handle success
    handleSuccess(response) {
        return response.data.data;
    }


    get(url, query = {}, headers = {}) {
        let options = this.getOptions(headers);
        url = buildUrl(url, query);

        return new Promise((resolve, reject) => {
            this.axiosInstance.get(url, options)
                .then((res) => resolve(this.handleSuccess(res)))
                .catch(err => reject(this.handleError(err)));
        });
    }

    post(url, query = {}, body = {}, headers = {}) {
        let options = this.getOptions(headers);
        url = buildUrl(url, query);

        return new Promise((resolve, reject) => {
            this.axiosInstance.post(url, body, options)
                .then((res) => resolve(this.handleSuccess(res)))
                .catch(err => reject(this.handleError(err)));
        });
    }

    put(url, query = {}, body = {}, headers = {}) {
        let options = this.getOptions(headers);
        url = buildUrl(url, query);

        return new Promise((resolve, reject) => {
            this.axiosInstance.put(url, body, options)
                .then((res) => resolve(this.handleSuccess(res)))
                .catch(err => reject(this.handleError(err)));
        });
    }

    delete(url, query = {}, headers = {}) {
        let options = this.getOptions(headers);
        url = buildUrl(url, query);

        return new Promise((resolve, reject) => {
            this.axiosInstance.delete(url, options)
                .then((res) => resolve(this.handleSuccess(res)))
                .catch(err => reject(this.handleError(err)));
        })
    }

    postFile(url, query = {}, file, body = {}, headers = {}, onUploadProgress = null) {
        let formData = new FormData();
        formData.append('file-upload', file);

        for (let key in body) {
            formData.append(key, body[key]);
        }

        let options = this.getOptions(headers);
        if (onUploadProgress) {
            options.onUploadProgress = onUploadProgress;
        }

        url = buildUrl(url, query);

        return new Promise((resolve, reject) => {
            this.axiosInstance.post(url, formData, options)
            .then((res) => resolve(this.handleSuccess(res)))
            .catch(err => reject(this.handleError(err)));
        });
    }
}
