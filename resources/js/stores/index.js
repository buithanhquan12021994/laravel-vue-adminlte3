import Vue from 'vue'
import Vuex from 'vuex'
import Auth from './auth.store';
import StorageManageHelper from '../helpers/storageManage';
import { ApplicationLang } from '../plugins/settings';
import Route from '../routes';

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    Auth
  },
  state: {
    lang: StorageManageHelper.getStorage(ApplicationLang) || 'ja',
  },
  getters: {
    getCurrentLang: (state) => state.lang
  },
  mutations: {},
  actions: {
    changeLanguage({ state }, payload) {
        state.lang = payload;
        let storages = {};
        storages[ApplicationLang] = payload;
        StorageManageHelper.setStorage(storages);
    },
    storeSearchingQuery({ dispatch }) {
        console.log(dispatch);
        console.log(window.location.href);
    }
  }
})
