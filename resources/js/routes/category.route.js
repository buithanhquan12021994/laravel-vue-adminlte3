export default [
    {
        path: 'category',
        component: () => import(/* webpackChunkName: "category-layout"*/'../views/category/Layout.vue'),
        name: 'CategoryManagement',
        redirect: { name: 'SearchCategory' },
        children: [
            {
                path: 'search',
                name: 'SearchCategory',
                meta: {
                    title: 'Search category page',
                    requireAuth: true,
                },
                component: () => import(/* webpackChunkName: "search-category-view" */'../views/category/Search.vue')
            },
            {
                path: 'edit/:id',
                name: 'EditCategory',
                meta: {
                    title: 'Edit Catgory',
                    requireAuth: true
                },
                component: () => import(/* webpackChunkName: "edit-category-view" */'../views/category/Edit.vue')
            },
            {
                path: 'create',
                name: 'CreateCategory',
                meta: {
                    title: 'Create new Catgory',
                    requireAuth: true
                },
                component: () => import(/* webpackChunkName: "edit-category-view" */'../views/category/Create.vue')
            }
        ]
    }
];
