import UsersRoute from './user.route';
import CategoryRoute from './category.route';

export default [
    {
        path: '/admin',
        component: () => import(/* webpackChunkName: "main-layout" */'../layout/Main.vue'),
        redirect: { name: 'Dashboard' },
        children: [
            {
                path: 'dashboard',
                name: 'Dashboard',
                meta: {
                    title: 'Dashboard page',
                    requireAuth: true
                },
                component: () => import(/* webpackChunkName: "dashboard-view" */'../views/Dashboard.vue'),
            },
            {
                path: 'profile',
                name: 'Profile',
                meta: {
                    title: 'Profile page',
                    requireAuth: true
                },
                component: () => import(/* webpackChunkName: "profile-view" */'../views/Profile.vue')
            },
            {
                path: 'developer',
                name: 'Developer',
                meta: {
                    title: 'Developer page',
                    requireAuth: true
                },
                component: () => import(/* webpackChunkName: "developer-view" */'../views/Developer.vue')
            },
            {
                path: 'products',
                name: 'Products',
                meta: {
                    title: 'Product page',
                    requireAuth: true,
                },
                component: () => import(/* webpackChunkName: "product-view" */'../views/product/Products.vue')
            },
            {
                path: 'product/tag',
                name: 'ProductTag',
                meta: {
                    title: 'Product tag page',
                    requireAuth: true
                },
                component: () => import(/* webpackChunkName: "product-tag-view" */'../views/product/Tag.vue')
            },
            {
                path: 'product/category',
                name: 'ProductCategory',
                meta: {
                    title: 'Product category page',
                    requireAuth: true,
                },
                component: () => import(/* webpackChunkName: "category-view" */'../views/product/Category.vue')
            },
            ...UsersRoute,
            ...CategoryRoute,
        ]
    },
]
