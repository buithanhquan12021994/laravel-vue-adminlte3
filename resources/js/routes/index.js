import VueRouter from "vue-router";
import Vue from 'vue';
import AdminRoute from './admin.route'
import AuthRoute from './auth.route';
import Store from '../stores'

Vue.use(VueRouter);

const routes = [
    ...AuthRoute,
    ...AdminRoute,

    {
        path: '*',
        redirect: { name: 'Dashboard' }
    }
];

const router = new VueRouter({
    mode: "history",
    routes,
});

// before loading route
router.beforeEach((to, from, next) => {
  document.title = to.meta.title ? to.meta.title : '';
  next();
});

router.beforeEach(async (to, from, next) => {
  const isAuth =  Store.getters['Auth/isAuth'];

  if (to.matched.some((res) => res.meta.requireAuth)) {
    if (isAuth) {
        next();
    } else {
        next({ name: 'Login' });
    }
  } else {
    next();
  }
})


export default router;
