export default [
    {
        path: 'users',
        component: () => import(/* webpackChunkName: "user-layout"*/'../views/user/Layout.vue'),
        name: 'Users',
        redirect: { name: 'SearchUser' },
        children: [
            {
                path: 'search',
                name: 'SearchUser',
                meta: {
                    title: 'Search user page',
                    requireAuth: true,
                },
                component: () => import(/* webpackChunkName: "search-user-view" */'../views/user/Search.vue')
            },
            {
                path: 'create',
                name: 'CreateUser',
                meta: {
                    title: 'Create new user page',
                    requireAuth: true,
                },
                component: () => import(/* webpackChunkName: "create-user-view" */'../views/user/Create.vue')
            },
            {
                path: 'edit/:userId',
                name: 'EditUser',
                meta: {
                    title: 'Edit user page',
                    requireAuth: true,
                },
                component: () => import(/* webpackChunkName: "edit-user-view" */'../views/user/Edit.vue')
            }
        ]
    }
];
