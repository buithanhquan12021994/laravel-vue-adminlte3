import Constants from '../plugins/constants';
import StorageManage from './storageManage';
import { ApplicationStorageToken } from '../plugins/settings';

export function buildHeaders(headers) {
    let requestHeaders = getDefaultRequestHeaders();

    if (!headers) {
        return requestHeaders;
    }
    if (Array.isArray(headers)) {
        return requestHeaders;
    }
    for (let keyname in headers) {
        requestHeaders[keyname.toLowerCase()] = headers[keyname];
    }

    return requestHeaders;
}

function getDefaultRequestHeaders() {
  return {
    page: 1,
    timeout: Constants.defaultRequestTimeOut,
    "Content-Type": "application/json",
    "Authorization": "Bearer " + StorageManage.getStorage(ApplicationStorageToken),
  }
}

export function buildUrl(url, query) {
  if (!query || query === {} || Array.isArray(query)) {
    return url;
  }
  for (const keyname in query) {
    if (typeof query[keyname] == 'object') {
      continue;
    }
    const param = `{${keyname}}`;
    while (url.includes(param)) {
      url = url.replace(param, query[keyname]);
    }
  }
  const queries = [];
  for (const keyname in query) {
    if (typeof query[keyname] !== 'object') {
      queries.push(`${keyname}=${query[keyname]}`);
      continue;
    }
    if (Array.isArray(query[keyname])) {
      queries.push(`${keyname}=${query[keyname].join(',')}`);
    } else {
      queries.push(`${keyname}=${JSON.stringify(query[keyname])}`);
    }
  }
  return `${url}?${queries.join('&')}`;
}


// parse Validation Message from laravel
export function parseValidationMessage(listMessage) {
    if (typeof listMessage != 'object') {
        throw new Error('Parameter type should be an array!');
        return;
    }
    let validation = {};
    for (let key in listMessage) {
        validation[key] = listMessage[key][0];
    }
    return validation;
}
