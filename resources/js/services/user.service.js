import ApiGateway from "../plugins/gateways";
import HttpClient  from "../plugins/httpClient";

var httpClient = new HttpClient(ApiGateway.localhost)
const prefix = '/user';

export default {
  search: (query) => httpClient.get(`${prefix}`, query),
  create: (body) => httpClient.post(`${prefix}`, {}, body),
  delete: (userId) => httpClient.delete(`${prefix}/${userId}`), // /user/1
  edit: (userId) => httpClient.get(`${prefix}/${userId}/edit`), // /user/1/edit
  update: (userId, body) => httpClient.put(`${prefix}/${userId}`, {}, body)
}
