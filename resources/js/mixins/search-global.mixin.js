import { buildUrl } from '../helpers/common';
import _ from 'lodash';

export default {
    watch: {
        '$route': {
            deep: true,
            handler() {
                this.initalizeUrlQuery()
                this.searchList();   // should define in component if using this mixin
                window.setSearchingRoute(this.currentRouteName)
            }
        },
    },
    mounted() {
        this.initalizeUrlQuery();
        this.searchList();
        window.setSearchingRoute(this.currentRouteName)
    },
    data() {
        return {
            currentFullPath: null,
            currentPath: null,
            currentRouteName: null,
            currentQuery: {},
            // list item searching
            listItems: [],

            // pagination
            page: 1,
            total: null,
            perPage: 0,
        }
    },
    methods: {
        searching() {
            if (this.currentQuery.page) {
                delete this.currentQuery.page;
            }

            if (!_.isEqual(this.form, this.currentQuery)) { // compare old and new
                this.currentQuery.page = 1;
            } else {
                this.currentQuery.page = this.page;
            }
            this.currentQuery = { ...this.form, page: this.currentQuery.page };

            if (!this.checkSameUrl()) {
                this.changeQueryRouteVue()
            }
        },

        checkSameUrl() {
            let buildQueryUrlPath = buildUrl(this.currentPath, this.currentQuery);
            return this.currentFullPath === buildQueryUrlPath
        },

        initalizeUrlQuery() {
            this.currentFullPath = this.$route.fullPath;
            this.currentPath = this.$route.path;
            this.currentRouteName = this.$route.name;
            this.currentQuery = this.$route.query;
            if (this.currentQuery.page) {
                this.page = this.currentQuery.page
            }
        },

        changeQueryRouteVue() {
            let query = Object.assign({}, this.currentQuery)
            this.$router.replace({ query });
        },

        changePage(changePageNumber) {
            this.page = changePageNumber;
            this.currentQuery = {
                ...this.form, page: this.page
            }
            this.searching();
            //let querybuilder =  buildUrl(this.currentPath, this.currentQuery);
            // console.log(querybuilder);
            // let query = Object.assign({}, this.currentQuery)
            // this.$router.replace({ query });
            // this.searchList();
        },
        onReset(e) {
            e.preventDefault();
            for(let prop in this.form) {
                this.form[prop] = '';
            }
        }
    }
}
