export default {
    label_email: "Eメール",
    label_name: "名前",
    label_create_new_user: "新しいユーザーを作成する",
    label_please_choose: "選択してください",
    label_no_data: "表示する情報がありません",
}
