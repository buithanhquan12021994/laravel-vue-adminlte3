// import Auth from './auth';
// import Panel from './panel';
// import User from './user';

// export default {
//     "auth": Auth,
//     "panel": Panel,
//     "user": User
// }

let instance = {};

const requireComponent = require.context(
  './',
  true,
  /\w+.js$/
)

requireComponent.keys().forEach(fileComponent => {
  const moduleConfiguration = requireComponent(fileComponent)
  const fileName= fileComponent.replace(/^\.\/(.*\/)*(.*)\.js$/, '$2');
  if (!fileName.includes('index')) {
    instance[fileName] = moduleConfiguration.default
  }
})

export default instance;
