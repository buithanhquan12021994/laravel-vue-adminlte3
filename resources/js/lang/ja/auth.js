export default {
    label_login: 'ログインページ',
    label_register: '登録',
    label_remind_register: "アカウントをお持ちではありませんか？",
    label_forgot_password: 'パスワードをお忘れですか',
    label_sign_in: 'ログイン'
}
