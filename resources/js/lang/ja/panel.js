export default {
    label_search: "検索",
    label_reset: "リセット",
    label_no_data: "表示する情報がありません",
    label_please_choose: "選択してください",
    label_no_data: "表示する情報がありません",
    label_register: "登録",
    label_go_back: "戻る",
    label_create: "試験追加",
    label_confirm_text_save: "登録します。よろしいですか？",
    label_confirm_text_remove: "削除します。よろしいですか？",
    label_confirm_title: "確認",
    label_edit: "編集",
    label_remove: "削除",
    label_delete_btn_modal: "削除する",
    label_cancel_btn_modal: "キャンセル"
}
