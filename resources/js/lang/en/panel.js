export default {
    label_search: "Search",
    label_reset: "Reset",
    label_no_data: "List is empty! No results...",
    label_please_choose: "Please select an option",
    label_no_data: "No information to display",
    label_register: "Create",
    label_go_back: "Back",
    label_create: "Create new",
    label_confirm_text_save: "I will register. Is it OK?",
    label_confirm_text_remove: "Delete. Is it ok?",
    label_confirm_title: "Confirm",
    label_edit: "Edit",
    label_remove: "Remove",
    label_delete_btn_modal: "Delete",
    label_cancel_btn_modal: "Cancel"
}
