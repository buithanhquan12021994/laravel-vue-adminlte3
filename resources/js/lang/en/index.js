let instance = {};

const requireComponent = require.context(
  './',
  true,
  /\w+.js$/
)

requireComponent.keys().forEach(fileComponent => {
  const moduleConfiguration = requireComponent(fileComponent)
  const fileName= fileComponent.replace(/^\.\/(.*\/)*(.*)\.js$/, '$2');
  if (!fileName.includes('index')) {
    instance[fileName] = moduleConfiguration.default
  }
})

export default instance;
