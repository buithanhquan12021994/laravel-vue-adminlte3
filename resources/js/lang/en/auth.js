export default {
    label_login: "Login Page",
    label_register: "Register",
    label_remind_register: "Don't have an account?",
    label_forgot_password: "Forgot your password",
    label_sign_in: "Sign in"
}
