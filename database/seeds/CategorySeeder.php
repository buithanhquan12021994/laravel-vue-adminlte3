<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $max = 20;
        for ($i = 1; $i <= $max; $i ++) {
            factory(Category::class, 1)->create();
        }
    }
}
