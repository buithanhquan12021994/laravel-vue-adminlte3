<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::insert([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'role' => 1,
            'password' => Hash::make(123)
        ]);
    }
}
