<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\ConstantModel;
use Illuminate\Support\Facades\DB;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $permission_id = Permission::insertGetId([
            'title' => 'admin_menu_access'
        ]);
        DB::table('permission_role')->insert([
            'role_id' => ConstantModel::ADMIN,
            'permission_id' => $permission_id
        ]);
    }
}
