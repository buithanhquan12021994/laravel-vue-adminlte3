<?php

use Illuminate\Database\Seeder;
use App\Models\ConstantModel;
use App\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roles = [
            [
                'id'    => ConstantModel::ADMIN,
                'title' => 'Admin',
            ],
            [
                'id'    => ConstantModel::MEMBER,
                'title' => 'Member',
            ],
        ];

        Role::insert($roles);
    }
}
