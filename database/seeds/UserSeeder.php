<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $max = 20;
        for ($i = 1; $i <= $max; $i ++) {
            factory(App\Models\User::class, 1)->create();
        }
    }
}
