<?php

use Illuminate\Database\Seeder;
use App\Models\Products;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $max = 20;
        for ($i = 1; $i <= $max; $i ++) {
            factory(Products::class, 2)->create();
        }
    }
}
