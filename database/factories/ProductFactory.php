<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Products;
use App\Models\Category;
use Faker\Generator as Faker;

$factory->define(Products::class, function (Faker $faker) {
    return [
        //
        'name' => "Product - " . $faker->name,
        'description' => "Description - ". $faker->name,
        'price' => $faker->randomNumber(4),
        'category_id' => Category::inRandomOrder()->first()->id
    ];
});
