<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Requests\Api\Category\UpdateCategoryRequest;
use App\Http\Requests\Api\Category\CreateCategoryRequest;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $categorySearching = new Category();
        $name = $request->query('name');
        $description = $request->query('description');

        $categorySearching->where('name', 'LIKE', '%'.$name.'%')->orWhere('description', 'LIKE', "%$description%");

        return response()->success(
            'Get list category',
            $categorySearching->orderBy('id', 'DESC')->paginate(10)
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        //
        $instance = new Category();
        $instance->fill($request->all());
        $instance->save();

        return response()->success(
            'create new',
            $instance
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $instance = (new Category())->find($id);
        return response()->success(
            'get detail',
            $instance
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $instance = (new Category())->find($id);
        return response()->success(
            'get detail',
            $instance
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, $id)
    {
        //
        $instance = (new Category())->find($id);
        $instance->name = $request->name;
        $instance->description = $request->description;
        $instance->save();
        return response()->success(
            'update success',
            true
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $instance = (new Category())->find($id);
        $instance->delete();
        return response()->success(
            'delete category',
            true
        );
    }
}
