<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Http\Requests\Api\Auth\LoginRequest;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * @OA\Post(
     *      path="/api/auth/login",
     *      operationId="Login",
     *      summary="User login",
     *      tags={"Authenticate"},
     *      description="Login by username or email and with password",
     *      @OA\RequestBody(
     *          required=true,
     *          description="Login Account",
     *          @OA\JsonContent(
     *              required={"email", "password"},
     *              @OA\Property(property="email", type="email", example="btquan@a2ds.io"),
     *              @OA\Property(property="password", type="string"),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="data", type="string" ),
     *          ),
     *      ),
     *      @OA\Response(response=400, description="Bad request"),
     *      security={
     *          {"api_key_security_example": {}}
     *      }
     *    )
     *
     * Returns list of projects
     */
    public function login(LoginRequest $request)
    {
        $token = (new User)->signIn($request);
        if (!$token) {
            return response()->unauthorized('Authentication failed!');
        }
        return $this->createNewToken($token);
    }

    /**
     * @OA\Post(
     *      path="/api/auth/register",
     *      operationId="Register",
     *      summary="Register new account",
     *      tags={"Authenticate"},
     *      description="Register a new account with name, email, password",
     *      @OA\RequestBody(
     *          required=true,
     *          description="Register account",
     *          @OA\JsonContent(
     *              required={"name", "email", "password"},
     *              @OA\Property(property="name", type="text", example="btquan"),
     *              @OA\Property(property="email", type="email", example="btquan@a2ds.io"),
     *              @OA\Property(property="password", type="string"),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="data", type="object" ),
     *          ),
     *      ),
     *      @OA\Response(response=400, description="Bad request"),
     *      security={
     *          {"api_key_security_example": {}}
     *      }
     *    )
     *
     * Returns list of projects
     */

    public function register(RegisterRequest $request)
    {
        $userInstance = (new User())->registerNewAccount($request);
        // auto login
        $token = auth()->login($userInstance);
        return $this->createNewToken($token);
    }

    /**
     * @OA\Post(
     *      path="/api/auth/logout",
     *      operationId="Logout",
     *      summary="Logout account",
     *      tags={"Authenticate"},
     *      description="Logout account",
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="data", type="object" ),
     *          ),
     *      ),
     *      @OA\Response(response=400, description="Bad request"),
     *      security={
     *          {"api_key_security_example": {}}
     *      }
     *    )
     *
     * Returns list of projects
     */
    public function logout()
    {
        auth()->logout();

        return response()->success('Logout successfully');
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token)
    {
        return response()->success(
            'Get token success',
            [
                'user_data' => auth()->user(),
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => config('jwt.ttl')
            ]
        );
    }
}
