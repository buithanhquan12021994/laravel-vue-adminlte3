<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('success', function ($message = '', $data = [], $statusCode = 200) {
            return Response::json([
              'status'  => 'success',
              'data' => $data,
              'message' => $message,
              'code' => $statusCode
            ], $statusCode);
        });

        Response::macro('fail', function ($message = '', $data = [], $statusCode = 400) {
            return Response::json([
              'status'  => 'fail',
              'data' => $data,
              'message' => $message,
              'code' => $statusCode
            ], $statusCode);
        });

        Response::macro('error', function ($message = '', $statusCode = 400) {
            return Response::json([
              'status'  => 'error',
              'message' => $message,
              'code' => $statusCode
            ], $statusCode);
        });

        Response::macro('unauthorized', function ($message = 'Unauthorized', $statusCode = 401) {
            return Response::json([
                'status' => 'fail',
                'message' => $message,
                'code' => $statusCode
            ], $statusCode);
        });
    }
}
