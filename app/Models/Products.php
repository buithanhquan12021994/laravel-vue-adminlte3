<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    //
    protected $table = 'products';

    // relationship
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
