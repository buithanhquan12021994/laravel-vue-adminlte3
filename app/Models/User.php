<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function registerNewAccount(Request $request)
    {
        $this->name = $request->name;
        $this->email = $request->email;
        $this->password = Hash::make($request->password);
        $this->save();

        return $this;
    }

    public function signIn(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $token = auth()->attempt(['email' => $email, 'password' => $password]);
        return $token;
    }

    public function search(Request $request)
    {
        $name = $request->query('name');
        $email = $request->query('email');
        $result = $this->where('name', 'LIKE', "%$name%")->orWhere('email', 'LIKE', "%$email%")->paginate(10);
        return $result;
    }

    public function createNew(Request $request)
    {
        $this->name = $request->name;
        $this->email = $request->email;
        $this->password = Hash::make($request->password);
        $this->role =  $request->role;
        $this->save();

        return $this;
    }

    public function removeUser($userId)
    {
        $instance = $this->find($userId);
        if (is_null($instance)) {
            return response()->fail(
                __('panel.fail'),
                'User is not found'
            );
        }
        $instance->delete();
        return response()->success(
            __('panel.delete_success'),
            ''
        );
    }

    public function updateUser($userId, $request)
    {
        $instance = $this->find($userId);
        if (is_null($instance)) {
            return response()->fail(
                __('panel.fail'),
                'User is not found'
            );
        }
        $instance->name = $request->name;
        $instance->password = Hash::make($request->password);
        $instance->email = $request->email;
        $instance->role = $request->role;
        $instance->save();
        return $instance;
    }
}
